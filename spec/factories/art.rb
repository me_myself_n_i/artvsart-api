FactoryGirl.define do
  factory :art do
    name "A Piece of Art"
    creator "A Fine Creator"
  end
end